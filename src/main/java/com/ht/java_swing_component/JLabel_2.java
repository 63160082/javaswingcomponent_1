/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ht.java_swing_component;

import java.awt.Frame;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JTextField;

/**
 *
 * @author ACER
 */
public class JLabel_2 extends Frame implements ActionListener {
    JTextField txt;
    JLabel lbl;
    JButton btn;
    
    JLabel_2() {
        txt = new JTextField();
        txt.setBounds(50, 50, 150, 20);
        
        lbl = new JLabel();
        lbl.setBounds(50, 100, 250, 20);
        
        btn = new JButton("Find IP");
        btn.setBounds(50, 150, 95, 30);
        btn.addActionListener(this);
        
        add(btn);
        add(txt);
        add(lbl);
        
        setSize(400, 400);
        setLayout(null);
        setVisible(true);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        try {
            String host = txt.getText();
            String ip = java.net.InetAddress.getByName(host).getHostAddress();
            lbl.setText("IP of" + host + "is : " + ip);
        }
        catch(Exception ex) {
            System.out.println(ex);
        }
    }
    
    public static void main(String[] args) {
        new JLabel_2();
    }
    
}
