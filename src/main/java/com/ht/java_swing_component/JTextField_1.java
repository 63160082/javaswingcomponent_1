/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ht.java_swing_component;

import javax.swing.JFrame;
import javax.swing.JTextField;

/**
 *
 * @author ACER
 */
public class JTextField_1 {
    public static void main(String[] args) {
        JFrame frame = new JFrame("TextField Ecample");
        JTextField txt1;
        JTextField txt2;
        txt1 = new JTextField("Welcome to Javatpoint");
        txt1.setBounds(50, 100, 200, 30);
        
        txt2 = new JTextField("AWT Tutorial");
        txt2.setBounds(50, 150, 200, 30);
        
        frame.add(txt1);
        frame.add(txt2);
        
        frame.setSize(400, 400);
        frame.setLayout(null);
        frame.setVisible(true);
    }
}
