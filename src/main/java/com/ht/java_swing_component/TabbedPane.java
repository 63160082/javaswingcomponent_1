/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ht.java_swing_component;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JTabbedPane;
import javax.swing.JTextArea;

/**
 *
 * @author ACER
 */
public class TabbedPane {
    JFrame frame;
    TabbedPane() {
        frame = new JFrame();
        JTextArea txtArea = new JTextArea(200, 200);
        JPanel p1 = new JPanel();
        p1.add(txtArea);
        
        JPanel p2 = new JPanel();
        JPanel p3 = new JPanel();
        
        JTabbedPane tp = new JTabbedPane();
        tp.setBounds(50, 50, 200, 200);
        tp.add("main", p1);
        tp.add("visit", p2);
        tp.add("help", p3);
        
        frame.add(tp);
        frame.setSize(400, 400);
        frame.setLayout(null);
        frame.setVisible(true);
    }

    public static void main(String[] args) {
        new TabbedPane();
    }
}
