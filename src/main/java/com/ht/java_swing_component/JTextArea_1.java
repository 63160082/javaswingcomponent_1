/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ht.java_swing_component;

import javax.swing.JFrame;
import javax.swing.JTextArea;

/**
 *
 * @author ACER
 */
public class JTextArea_1 {
    JTextArea_1() {
        JFrame frame = new JFrame();
        JTextArea area = new JTextArea("Welcome to javatpoint");
        area.setBounds(10, 30, 200, 200);
        frame.add(area);
        frame.setSize(300, 300);
        frame.setLayout(null);
        frame.setVisible(true);
    }
    
    public static void main(String[] args) {
        new JTextArea_1();
    }
}
