/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ht.java_swing_component;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JTextArea;

/**
 *
 * @author ACER
 */
public class Menu_2 implements ActionListener {
    JFrame frame;
    JMenuBar mb;
    JMenu file, edit, help;
    JMenuItem cut, copy, paste, selectAll;
    JTextArea txtArea;

    Menu_2() {
        frame = new JFrame();
        cut = new JMenuItem("cut");
        copy = new JMenuItem("copy");
        paste = new JMenuItem("paste");
        selectAll = new JMenuItem("selectAll");
        cut.addActionListener(this);
        copy.addActionListener(this);
        paste.addActionListener(this);
        selectAll.addActionListener(this);
        
        mb = new JMenuBar();
        file = new JMenu("File");
        edit = new JMenu("Edit");
        help = new JMenu("Help");
        
        edit.add(cut);
        edit.add(copy);
        edit.add(paste);
        edit.add(selectAll);
        
        mb.add(file);
        mb.add(edit);
        mb.add(help);
        
        txtArea = new JTextArea();
        txtArea.setBounds(5, 5, 360, 320);
        
        frame.add(mb);
        frame.add(txtArea);
        frame.setJMenuBar(mb);
        
        frame.setLayout(null);
        frame.setSize(400, 400);
        frame.setVisible(true);
    }
    @Override
    public void actionPerformed(ActionEvent e) {
        if (e.getSource() == cut) {
            txtArea.cut();
        }
        if (e.getSource() == paste) {
            txtArea.paste();
        }
        if (e.getSource() == copy) {
            txtArea.copy();
        }
        if (e.getSource() == selectAll)
            txtArea.selectAll();
    }
    
    public static void main(String[] args) {
        new Menu_2();
    }
    
}
