/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ht.java_swing_component;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPasswordField;
import javax.swing.JTextField;

/**
 *
 * @author ACER
 */
public class JPasswordField_2 {
    public static void main(String[] args) {
        JFrame frame = new JFrame("Password Field Example");
        
        final JLabel lbl = new JLabel();
        lbl.setBounds(20, 150, 200, 50);
        
        final JPasswordField value = new JPasswordField();
        value.setBounds(100, 75, 100, 30);
        
        JLabel lbl1 = new JLabel("Username:");
        lbl1.setBounds(20, 20, 80, 30);
        
        JLabel lbl2 = new JLabel("Password:");
        lbl2.setBounds(20, 75, 80, 30);
        
        JButton btn = new JButton("Login");
        btn.setBounds(100, 120, 80, 30);
        
        final JTextField txt = new JTextField();
        txt.setBounds(100, 20, 100, 30);
        
        frame.add(value);
        frame.add(lbl);
        frame.add(lbl1);
        frame.add(lbl2);
        frame.add(btn);
        frame.add(txt);
        
        frame.setSize(300, 300);
        frame.setLayout(null);
        frame.setVisible(true);
        
        btn.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                String data = "Username " + txt.getText();
                data += ", Password: " + new String(value.getPassword());
                lbl.setText(data);
            }
        });
    }
 
}
