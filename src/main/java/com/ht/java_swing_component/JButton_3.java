/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ht.java_swing_component;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;

/**
 *
 * @author ACER
 */
public class JButton_3 {
    JButton_3() {
        JFrame frame = new JFrame("Button Example");
        
        JButton btn = new JButton(new ImageIcon("D:\\icon.png"));
        btn.setBounds(100, 100, 100, 40);
        frame.add(btn);
        frame.setSize(300, 400);
        frame.setLayout(null);
        frame.setVisible(true);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    }
    
    public static void main(String[] args) {
        new JButton_3();
    }
}
