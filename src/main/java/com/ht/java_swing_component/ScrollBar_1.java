/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ht.java_swing_component;

import javax.swing.JFrame;
import javax.swing.JScrollBar;

/**
 *
 * @author ACER
 */
public class ScrollBar_1 {
    ScrollBar_1() {
        JFrame frame = new JFrame("Scrollbar Example");
        
        JScrollBar scrol = new JScrollBar();
        scrol.setBounds(100, 100, 50, 100);
        
        frame.add(scrol);
        frame.setSize(400, 400);
        frame.setLayout(null);
        frame.setVisible(true);
    }

    public static void main(String args[]) {
        new ScrollBar_1();
    }
}
