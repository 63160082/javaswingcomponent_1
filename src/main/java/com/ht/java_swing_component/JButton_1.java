/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ht.java_swing_component;

import javax.swing.JButton;
import javax.swing.JFrame;

/**
 *
 * @author ACER
 */
public class JButton_1 {
    public static void main(String[] args) {
        JFrame frame = new JFrame("Button Example");
        
        JButton btn = new JButton("Click Here");
        btn.setBounds(50, 100, 95, 30);
        frame.add(btn);
        frame.setSize(400, 400);
        frame.setLayout(null);
        frame.setVisible(true);
    }
}
