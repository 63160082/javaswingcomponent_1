/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ht.java_swing_component;

import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;

/**
 *
 * @author ACER
 */
public class Dialog {
    private static JDialog dia;
    Dialog() {
        JFrame f = new JFrame();
        dia = new JDialog(f, "Dialog Example", true);
        dia.setLayout(new FlowLayout());
        
        JButton b = new JButton("OK");
        b.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                Dialog.dia.setVisible(false);
            }
        });
        
        dia.add(new JLabel("Click button to continue."));
        dia.add(b);
        dia.setSize(300, 300);
        dia.setVisible(true);
    }

    public static void main(String args[]) {
        new Dialog();
    }
}
