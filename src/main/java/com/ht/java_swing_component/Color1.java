/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ht.java_swing_component;

import java.awt.Color;
import java.awt.Container;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JColorChooser;
import javax.swing.JFrame;

/**
 *
 * @author ACER
 */
public class Color1 extends JFrame implements ActionListener {
    JButton btn;
    Container ctn;
    Color1() {
        ctn = getContentPane();
        ctn.setLayout(new FlowLayout());
        btn = new JButton("color");
        btn.addActionListener(this);
        ctn.add(btn);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        Color initialcolor = Color.RED;
        Color color = JColorChooser.showDialog(this, "Select a color", initialcolor);
        ctn.setBackground(color);
    }

    public static void main(String[] args) {
        Color1 ch = new Color1();
        ch.setSize(400, 400);
        ch.setVisible(true);
        ch.setDefaultCloseOperation(EXIT_ON_CLOSE);
    }

}
