/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ht.java_swing_component;

import java.awt.event.AdjustmentEvent;
import java.awt.event.AdjustmentListener;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JScrollBar;

/**
 *
 * @author ACER
 */
public class ScrollBar_2 {
    ScrollBar_2() {
        JFrame frame = new JFrame("Scrollbar Example");
        
        final JLabel lbl = new JLabel();
        lbl.setHorizontalAlignment(JLabel.CENTER);
        lbl.setSize(400, 100);
        
        final JScrollBar s = new JScrollBar();
        s.setBounds(100, 100, 50, 100);
        
        frame.add(s);
        frame.add(lbl);
        frame.setSize(400, 400);
        frame.setLayout(null);
        frame.setVisible(true);
        
        s.addAdjustmentListener(new AdjustmentListener() {
            @Override
            public void adjustmentValueChanged(AdjustmentEvent e) {
                lbl.setText("Vertical Scrollbar value is:" + s.getValue());
            }
        });
    }

    public static void main(String args[]) {
        new ScrollBar_2();
    }
}
