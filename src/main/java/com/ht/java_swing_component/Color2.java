/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ht.java_swing_component;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JColorChooser;
import javax.swing.JFrame;
import javax.swing.JTextArea;

/**
 *
 * @author ACER
 */
public class Color2 extends JFrame implements ActionListener {
    JFrame frame;
    JButton btn;
    JTextArea txtArea;
    Color2() {
        frame = new JFrame("Color Chooser Example.");
        
        btn = new JButton("Pad Color");
        btn.setBounds(200, 250, 100, 30);
        
        txtArea = new JTextArea();
        txtArea.setBounds(10, 10, 300, 200);
        
        btn.addActionListener(this);
        
        frame.add(btn);
        frame.add(txtArea);
        
        frame.setLayout(null);
        frame.setSize(400, 400);
        frame.setVisible(true);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        Color c = JColorChooser.showDialog(this, "Choose", Color.CYAN);
        txtArea.setBackground(c);
    }

    public static void main(String[] args) {
        new Color2();
    }

}
