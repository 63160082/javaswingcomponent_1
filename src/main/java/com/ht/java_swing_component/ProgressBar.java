/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ht.java_swing_component;

import javax.swing.JFrame;
import javax.swing.JProgressBar;

/**
 *
 * @author ACER
 */
public class ProgressBar extends JFrame {
    JProgressBar PBar;
    int i = 0, num = 0;

    ProgressBar() {
        PBar = new JProgressBar(0, 2000);
        PBar.setBounds(40, 40, 160, 30);
        PBar.setValue(0);
        PBar.setStringPainted(true);
        add(PBar);
        setSize(250, 150);
        setLayout(null);
    }

    public void iterate() {
        while (i <= 2000) {
            PBar.setValue(i);
            i = i + 20;
            try {
                Thread.sleep(150);
            } catch (Exception e) {
            }
        }
    }

    public static void main(String[] args) {
        ProgressBar m = new ProgressBar();
        m.setVisible(true);
        m.iterate();
    }
}
